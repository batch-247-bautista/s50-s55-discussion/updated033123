import React from 'react';

// Initializes a React Context
// Creates a Context object
// A contxt object as teh name stress is a data type of an object that can be used to store information that can be shared to other components within the app
// The context object is a differenta approach to passing information between components and allows easier access by avoiding the value of prop-drilling
	// Pop=Drilling is a term we use whenever we pass information from one component to another using props 
const UserContext = React.createContext();

// Initializes a Context Provider
// Give us availability to provide a specific context through component
// The 'Provider' component allows other components to consume/use/utilize/facilitate context object and supply the necessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;